package com.epam;

import org.apache.logging.log4j.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class App {
    private static Logger logger1 = LogManager.getLogger(App.class);


    public static void main(String[] args) {

//        ExampleSMS.send("Test sms");

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String answer = null;

        do {
            try {

                logger1.trace("This is a trace message");
                logger1.debug("This is a debug message");
                logger1.info("This is an info message");
                logger1.warn("This is a warn message");
                logger1.error("This is an error message");
                logger1.fatal("This is a fatal message");

                answer = br.readLine().toUpperCase();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } while (!answer.equals("STOP"));



    }

}
