package com.epam;

import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;

public class ExampleSMS {

    public static final String ACCOUNT_SID = "AC21b135bb1b6a4083f5cb3a883ef410a4";
    public static final String AUTH_TOKEN = "b3a9625d4d4d5280f4393e5200a54995";


    public static void send(String str) {
        Twilio.init(ACCOUNT_SID, AUTH_TOKEN);
        Message message = Message
                .creator(new PhoneNumber("+380961650321"),
                        new PhoneNumber("+19282770963"), str).create();
    }
}
